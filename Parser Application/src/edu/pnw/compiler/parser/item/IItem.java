package edu.pnw.compiler.parser.item;

import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;

public interface IItem {

  abstract IProduction getProduction();

  abstract int getDotLocation();

  abstract ISymbol getDotSymbol();

  abstract String toString();

  abstract ISymbol getBeforeDotSymbol();

}
