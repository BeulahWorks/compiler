package edu.pnw.compiler.parser.item;

import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;

public class SetOfItems implements ISetOfItems {
  private LinkedList<IItem> itemlist;

  public SetOfItems() {
    itemlist = new LinkedList<>();
  }

  @Override
  public LinkedList<IItem> getSetOfItems() {
    return itemlist;
  }

  @Override
  public void addItem(IItem item) {
    this.itemlist.add(item);
  }

  @Override
  public boolean findItemInSet(IProduction production, int dotLocaiton) {
    boolean hasItem = false;
    for (IItem item : itemlist) {
      if (item.getProduction().equals(production) && item.getDotLocation() == dotLocaiton) {
        hasItem = true;
      }
    }
    return hasItem;
  }

  @Override
  public LinkedList<IItem> findItemsBySymbol(ISymbol symbol) {
    LinkedList<IItem> resultItemList = new LinkedList<>();
    for (IItem item : itemlist) {
      ISymbol dotSymbol = item.getDotSymbol();
      if(dotSymbol!=null){
        if (dotSymbol.equals(symbol)) {
          resultItemList.add(item);
        }
      }
    }
    return resultItemList;
  }

  @Override
  public boolean compare(ISetOfItems newSetOfItems) {
    // Only compare the first item in the set.
    IItem item = newSetOfItems.getSetOfItems().get(0);
    if (this.findItemInSet(item.getProduction(), item.getDotLocation())) {
      return true;
    }

    return false;
  }

  @Override
  public String[] printItems() {
    String[] setOfItemsString = new String[itemlist.size()];
    int itemIndex = 0;
    for (IItem item : itemlist) {
      setOfItemsString[itemIndex] = item.toString();
      itemIndex++;
    }
    return setOfItemsString;
  }
}
