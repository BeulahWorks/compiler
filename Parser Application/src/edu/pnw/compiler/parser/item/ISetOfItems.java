package edu.pnw.compiler.parser.item;

import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;

public interface ISetOfItems {

  abstract void addItem(IItem item);

  abstract LinkedList<IItem> getSetOfItems();

  abstract boolean findItemInSet(IProduction production, int dotLocaiton);

  abstract LinkedList<IItem> findItemsBySymbol(ISymbol symbol);

  abstract boolean compare(ISetOfItems newSetOfItems);

  abstract String[] printItems();

}
