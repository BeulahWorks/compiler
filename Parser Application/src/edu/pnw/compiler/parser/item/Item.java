package edu.pnw.compiler.parser.item;

import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;

public class Item implements IItem {
  private final IProduction production;

  private final int dotLocation;

  public Item(IProduction production, int dotLocation) throws Exception {

    if (dotLocation > production.getAllProductionContent().size()) {
      throw new Exception("Invalid dot location");
    }
    this.production = production;
    this.dotLocation = dotLocation;
  }

  @Override
  public IProduction getProduction() {
    return production;
  }

  @Override
  public int getDotLocation() {
    return dotLocation;
  }
  @Override
  public ISymbol getDotSymbol() {
    if(dotLocation != production.getAllProductionContent().size()){
      return production.getAllProductionContent().get(dotLocation);
    }
    return null;
  }
  @Override
  public ISymbol getBeforeDotSymbol() {
	return production.getSingleProductionContent(dotLocation - 1);
}
  @Override
  public String toString(){
    String itemString = production.getGoalSymbol().getSymbolVaule();
    LinkedList<ISymbol> productionList = production.getAllProductionContent();
    itemString += "->";
    for(int i = 0; i < dotLocation; i++){
      itemString += productionList.get(i).getSymbolVaule();
    }
    
    itemString += ".";
    int size = production.getAllProductionContent().size();
    for(int i = dotLocation; i <size; i++) {
      itemString += productionList.get(i).getSymbolVaule();
    }
    return itemString;
  }
}
