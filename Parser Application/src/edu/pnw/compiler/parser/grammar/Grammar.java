package edu.pnw.compiler.parser.grammar;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rui
 *
 */
public class Grammar implements IGrammar {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  /**
   * symbol map.
   */
  private HashMap<String, ISymbol> symbolTable;

  /**
   * Non-Terminal symbol list.
   */
  private LinkedList<ISymbol> nonTerminalSymbol;
  /**
   * Terminal symbol list.
   */
  private LinkedList<ISymbol> termianlSymbol;
  /**
   * Production list.
   */
  private LinkedList<IProduction> productionList;

  public Grammar() {
    nonTerminalSymbol = new LinkedList<ISymbol>();
    termianlSymbol = new LinkedList<ISymbol>();
    productionList = new LinkedList<IProduction>();
    symbolTable = new HashMap();
  }

  @Override
  public boolean addSymbolToTable(String symbolValue, ISymbol symbol) {
    symbolTable.put(symbolValue, symbol);
    return true;
  }

  /**
   * Add new production into the production list.
   * 
   * @param production new production
   */
  @Override
  public boolean addProduction(IProduction production) {
    if (production != null) {
      productionList.add(production);
      return true;
    } else {
      return false;
    }

  }

  /**
   * @param nonTerminalSymbol new Non-Terminal symbol.
   */
  @Override
  public boolean addNonTerminalSymbol(ISymbol nonTerminalSymbol) {
    if (nonTerminalSymbol.getSymbolType().equals(Symbol.Type.NonTerminal)) {
      this.nonTerminalSymbol.add(nonTerminalSymbol);
      return true;
    } else {
      logger.info("Input symbol is not non-terminal");
      return false;
    }

  }

  @Override
  public boolean addTerminalSymbol(ISymbol terminalSymbol) {
    if (terminalSymbol.getSymbolType().equals(Symbol.Type.Terminal)) {
      this.termianlSymbol.add(terminalSymbol);
      return true;
    } else {
      logger.info("Input symbol is not terminal");
      return false;
    }
  }

  /**
   * @return Non-terminal symbol in the grammar.
   */
  @Override
  public LinkedList<ISymbol> getNonTerminalSymbol() {
    return nonTerminalSymbol;
  }

  /**
   * @return Terminal symbol in the grammar.
   */
  @Override
  public LinkedList<ISymbol> getTermianlSymbol() {
    return termianlSymbol;
  }

  /**
   * @return production list in the grammar.
   */
  @Override
  public LinkedList<IProduction> getProductionList() {
    return productionList;
  }

  /**
   * @return HashTable of Symbol.
   */
  @Override
  public HashMap<String, ISymbol> getSymbolTable() {
    return symbolTable;
  }

  @Override
  public String[] printProductions() {
    logger.info("Print Grammar:");
    String[] productionString = new String[productionList.size()];
    int indexOfProductionString = 0;
    for (IProduction production : productionList) {
      productionString[indexOfProductionString] = production.getGoalSymbol().getSymbolVaule();
      productionString[indexOfProductionString] += "->";
      for (ISymbol symbol : production.getAllProductionContent()) {
        productionString[indexOfProductionString] += symbol.getSymbolVaule();
      }
      indexOfProductionString++;
    }
    return productionString;
  }

  @Override
  public LinkedList<IProduction> findProductionsByGoalSymbol(ISymbol goalSymbol) {
    LinkedList<IProduction> resultProductions = new LinkedList<>();
    // The return list including reference of production in the grammar.
    for (IProduction production : this.productionList) {
      if (production.getGoalSymbol().equals(goalSymbol)) {
        resultProductions.add(production);
      }
    }
    return resultProductions;
  }

  @Override
  public IProduction findProduction(ISymbol symbol) {
    logger.info("Try to find production of Symbol: " + symbol.getSymbolVaule());
    for (IProduction production : this.productionList) {
      if (production.getGoalSymbol().equals(symbol)) {
        logger.info("Find production of Symbol: " + symbol.getSymbolVaule() + " Production: "
            + production.toString());
        return production;
      }
    }
    logger.info("Do not find production of Symbol: " + symbol.getSymbolVaule());
    return null;
  }

  @Override
  public LinkedList<ISymbol> getAllSymbol() {
    LinkedList<ISymbol> allSymbol = new LinkedList<>();
    for (int i = 1; i < nonTerminalSymbol.size(); i++) {
      allSymbol.add(nonTerminalSymbol.get(i));
    }
    allSymbol.addAll(termianlSymbol);
    return allSymbol;
  }

  @Override
  public boolean findSymbol(String symbolString) {
    return this.symbolTable.containsKey(symbolString);
  }

  @Override
  public int getIndexOfProduction(IProduction targetProduction) {
    int productionIndex = 0;
    for (IProduction production : this.productionList) {
      if (targetProduction.equals(production)) {
        return productionIndex;
      }
      productionIndex++;
    }
    return -1;
  }

  @Override
  public ISymbol getParentSymbol(ISymbol symbol) {
    for (IProduction production : productionList) {
      // if production content is only one symbol,
      // and the symbol is same as input symbol.
      if (production.getAllProductionContent().size() == 1
          && production.getAllProductionContent().get(0).equals(symbol)) {
        return production.getGoalSymbol();
      }
    }
    return null;
  }

  @Override
  public IProduction getProductionByIndex(int operatorIndex) {
    return productionList.get(operatorIndex - 1);
  }
}
