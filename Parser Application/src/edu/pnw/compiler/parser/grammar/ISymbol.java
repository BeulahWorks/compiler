package edu.pnw.compiler.parser.grammar;

import edu.pnw.compiler.parser.grammar.Symbol.Type;

public interface ISymbol {
  abstract String getSymbolVaule();
  abstract Type getSymbolType();
}
