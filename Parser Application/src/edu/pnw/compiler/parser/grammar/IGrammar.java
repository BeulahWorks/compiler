package edu.pnw.compiler.parser.grammar;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;

public interface IGrammar {
  abstract boolean addSymbolToTable(String symbolValue, ISymbol symbol);

  abstract boolean addProduction(IProduction production);

  abstract boolean addNonTerminalSymbol(ISymbol nonTerminalSymbol);

  abstract boolean addTerminalSymbol(ISymbol terminalSymbol);

  abstract LinkedList<IProduction> getProductionList();

  abstract LinkedList<ISymbol> getTermianlSymbol();

  abstract LinkedList<ISymbol> getNonTerminalSymbol();

  abstract HashMap<String, ISymbol> getSymbolTable();

  abstract String[] printProductions();

  abstract LinkedList<IProduction> findProductionsByGoalSymbol(ISymbol goalSymbol);

  abstract IProduction findProduction(ISymbol symbol);

  abstract LinkedList<ISymbol> getAllSymbol();

  abstract boolean findSymbol(String symbolString);

  abstract int getIndexOfProduction(IProduction targetProduction);

  abstract ISymbol getParentSymbol(ISymbol symbol);

  abstract IProduction getProductionByIndex(int operatorIndex);
}
