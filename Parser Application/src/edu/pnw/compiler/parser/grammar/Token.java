package edu.pnw.compiler.parser.grammar;

public class Token implements IToken {
  public static enum Type {
    Id, Operator;
  }

  final Type type;
  final String value;

  public Token(Type type, String value) {
    this.type = type;
    this.value = value;
  }
}
