package edu.pnw.compiler.parser.grammar;

public class Symbol implements ISymbol {

  private final Type type;
  private final String value;

  public Symbol(Type type, String value) {
    this.type = type;
    this.value = value;
  }

  public static enum Type {
    Terminal, NonTerminal;
  }

  @Override
  public String getSymbolVaule() {
    return value;
  }

  @Override
  public Type getSymbolType() {
    return type;
  }
  @Override
  public String toString(){
    return value;
  }

}
