package edu.pnw.compiler.parser.grammar;

import java.util.LinkedList;

public class Production implements IProduction {
  final private LinkedList<ISymbol> productionContent;
  final private ISymbol goalSymbol;



  public Production(ISymbol goalSymbol, LinkedList<ISymbol> productionContent) {
    this.goalSymbol = goalSymbol;
    this.productionContent = productionContent;
  }

  @Override
  public LinkedList<ISymbol> getAllProductionContent() {
    return productionContent;
  }

  @Override
  public ISymbol getSingleProductionContent(int dotLocation) {
    if (dotLocation != productionContent.size()) {
      return productionContent.get(dotLocation);
    }
    return null;
  }

  @Override
  public ISymbol getGoalSymbol() {
    return goalSymbol;
  }

  @Override
  public String toString() {
    String itemString = this.goalSymbol.getSymbolVaule();
    itemString += "->";
    for (ISymbol symbol : this.productionContent) {
      itemString += symbol.getSymbolVaule();
    }
    return itemString;
  }

  @Override
  public ISymbol findSymbolAfter(ISymbol symbol) {
    int indexSymbolAfter;
    if(productionContent.contains(symbol)){
      indexSymbolAfter = productionContent.indexOf(symbol) + 1;
      if(indexSymbolAfter < productionContent.size()){
        return productionContent.get(indexSymbolAfter);
      }
    }
    return null;
  }
}
