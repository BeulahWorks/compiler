package edu.pnw.compiler.parser.grammar;

import java.util.LinkedList;

public interface IProduction {
  abstract LinkedList<ISymbol> getAllProductionContent();

  abstract ISymbol getGoalSymbol();

  /**
   * @param dotLocation the dot location in the Item.
   * @return the Symbol after dot, if there is;
   *         else return null.
   */
  abstract ISymbol getSingleProductionContent(int dotLocation);

  abstract ISymbol findSymbolAfter(ISymbol symbol);

}
