package edu.pnw.compiler.parser.generator;

import java.util.HashMap;

import edu.pnw.compiler.parser.grammar.ISymbol;

public interface IAction {

  abstract void addOperator(ISymbol symbol, ActionOperator actionOperator);

  abstract HashMap<ISymbol, IActionOperator> getActionMap();

  abstract String getActionString(ISymbol symbol);

  abstract boolean findActionBySymbol(ISymbol targetSymbol);


}
