package edu.pnw.compiler.parser.generator;

import edu.pnw.compiler.parser.generator.ActionOperator.OperatorType;

public interface IActionOperator {

  abstract OperatorType getOperatorType();

  abstract int getOperatorIndex();

}
