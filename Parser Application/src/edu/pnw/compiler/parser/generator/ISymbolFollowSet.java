package edu.pnw.compiler.parser.generator;

import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.ISymbol;

public interface ISymbolFollowSet {

  abstract void addFollowSymbol(ISymbol afterSymbol);

  abstract void inheritanceParentSet(ISymbolFollowSet symbolFollowParent);

  abstract LinkedList<ISymbol> getSymbolFollowList();

}
