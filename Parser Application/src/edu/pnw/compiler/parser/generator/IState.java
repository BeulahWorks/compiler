package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.item.IItem;
import edu.pnw.compiler.parser.item.ISetOfItems;

public interface IState {

  abstract void addAction(ISymbol currentSymbol, ActionOperator actionOperator);

  abstract void addGoto(ISymbol currentSymbol, ActionOperator actionOperator);

  abstract String toString(IGrammar grammar);

  abstract IAction getStateAction();

  abstract IGoto getStateGoto();

  abstract HashMap<ISymbol, IActionOperator> getStateActionMap();

  abstract HashMap<ISymbol, IActionOperator> getStateGotoMap();

}
