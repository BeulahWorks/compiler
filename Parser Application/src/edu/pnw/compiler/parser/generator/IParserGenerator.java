package edu.pnw.compiler.parser.generator;

import java.util.ArrayList;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.item.ISetOfItems;

public interface IParserGenerator {

	abstract void generateGrammar(String filename) throws Exception;

	abstract IGrammar getGrammar();

	abstract void itemGenerator(IGrammar grammar) throws Exception;

	abstract ISetOfItems gotoNewSetOfItems(ISetOfItems setOfItems, ISymbol symbol) throws Exception;

	abstract ISetOfItems closureSetOfItems(ISetOfItems setOfItems) throws Exception;

	abstract ISetOfItems getS0SetofItem() throws Exception;

	abstract LinkedList<ISetOfItems> getAllSetOfItemsCollector();

	abstract ITableDrivenParser getTableDrivenParser();

	abstract void generateTableDrivenParser(String parsingTable) throws Exception;

}
