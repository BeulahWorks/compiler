package edu.pnw.compiler.parser.generator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrammarFileReader {
  public Logger logger = LoggerFactory.getLogger(this.getClass());

  public LinkedList<String> readFile(String filename) {
    LinkedList<String> lineList = new LinkedList<>();
    try {
      BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        logger.info("read grammar " + line);
        lineList.add(line);
      }
    } catch (Exception error) {
      logger.error("Error when reading file.", error);
    }
    return lineList;
  }
}

