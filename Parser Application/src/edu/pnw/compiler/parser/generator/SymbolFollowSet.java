package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.ISymbol;

public class SymbolFollowSet implements ISymbolFollowSet {
  private LinkedList<ISymbol> symbolFollowList;
  @Override
  public LinkedList<ISymbol> getSymbolFollowList() {
    return symbolFollowList;
  }

  public SymbolFollowSet() {
    symbolFollowList = new LinkedList<>();
  }
  @Override
  public void addFollowSymbol(ISymbol afterSymbol) {
    if(!symbolFollowList.contains(afterSymbol)){
      symbolFollowList.add(afterSymbol);
    }  
  }
  @Override
  public void inheritanceParentSet(ISymbolFollowSet symbolFollowParent) {
    symbolFollowList.addAll(symbolFollowParent.getSymbolFollowList());
  }

  @Override
  public String toString(){
    return symbolFollowList.toString();
  }
}
