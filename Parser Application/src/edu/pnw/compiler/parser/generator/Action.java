package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.item.ISetOfItems;

public class Action implements IAction {

  HashMap<ISymbol, IActionOperator> actionMap;

  @Override
  public HashMap<ISymbol, IActionOperator> getActionMap() {
    return actionMap;
  }

  public Action() {
    actionMap = new HashMap<>();
  }

  @Override
  public void addOperator(ISymbol symbol, ActionOperator actionOperator) {
    //if(!actionMap.containsKey(symbol)){
      actionMap.put(symbol, actionOperator);
    //}
  }


  @Override
  public String getActionString(ISymbol targetSymbol) {
      if(findActionBySymbol(targetSymbol)){
        return actionMap.get(targetSymbol).toString();
      }
      return null;
  }
  
  @Override
  public boolean findActionBySymbol(ISymbol targetSymbol){
    for(ISymbol symbol : actionMap.keySet()){
      if(targetSymbol.equals(symbol)){
        return true;
      }
    }
    return false;
  }
}
