package edu.pnw.compiler.parser.generator;

import java.util.HashMap;

import edu.pnw.compiler.parser.grammar.ISymbol;

public interface IGoto {

  abstract void addOperator(ISymbol symbol, IActionOperator actionOperator);

  abstract HashMap<ISymbol, IActionOperator> getGotoMap();

  abstract String getGotoString(ISymbol symbol);

  abstract boolean findGotoBySymbol(ISymbol targetSymbol);

}
