package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.grammar.Symbol;
import edu.pnw.compiler.parser.item.IItem;
import edu.pnw.compiler.parser.item.ISetOfItems;

public class TableDrivenParserLR0 implements ITableDrivenParser {
  private LinkedList<ISetOfItems> allSetOfItemsCollector;
  private HashMap<Integer, IState> parsingTable;
  private IGrammar grammar;
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public void generateParser(LinkedList<ISetOfItems> allSetOfItemsCollector, IGrammar grammar)
      throws Exception {

    this.allSetOfItemsCollector = allSetOfItemsCollector;
    this.grammar = grammar;
    parsingTable = new HashMap<>();
    // current setOfItems.
    ISetOfItems currentSetOfItems;
    for (int i = 0; i < this.allSetOfItemsCollector.size(); i++) {
      currentSetOfItems = this.allSetOfItemsCollector.get(i);
      parsingTable.put(i, initialState(currentSetOfItems));
    }
  }

  private IState initialState(ISetOfItems currentSetOfItems) throws Exception {
    logger.info("Start to create new State.");
    IState newState = new State();
    ISymbol currentSymbol;
    for (IItem item : currentSetOfItems.getSetOfItems()) {
      currentSymbol = item.getDotSymbol();
      if (currentSymbol != null) {
        if (currentSymbol.getSymbolType().equals(Symbol.Type.Terminal)) {
          // if current item is terminal, set action function.
          logger.info("Create a shift action of Symbol " + currentSymbol.getSymbolVaule());
          //if (newState.getStateAction().getActionMap().isEmpty()) {
            newState.addAction(currentSymbol, new ActionOperator(ActionOperator.OperatorType.Shift,
                getIndexOfPointToSetOfItem(item)));
          //}
        } else {
          // if current item is non-terminal, set goto function
          logger.info("Create a goto of Symbol " + currentSymbol.getSymbolVaule());
          newState.addGoto(currentSymbol, new ActionOperator(ActionOperator.OperatorType.Goto,
              getIndexOfPointToSetOfItem(item)));
        }
      } else {
        // if dot is after $ symbol, the actions would be accept.
        if (item.getBeforeDotSymbol().equals(grammar.getSymbolTable().get("$"))) {
          for (ISymbol symbol : grammar.getTermianlSymbol()) {
            logger.info("Create a accept action of Symbol " + symbol.getSymbolVaule());
            newState.addAction(symbol, new ActionOperator(ActionOperator.OperatorType.Accept, -1));
          }
        } else {
          // if dot is after general symbol, the action would be reduce,
          // the reduce would return the production id, the id start from index 1.
          IProduction targetProduction = item.getProduction();
          int productionIndex = grammar.getIndexOfProduction(targetProduction);
          for (ISymbol symbol : grammar.getTermianlSymbol()) {
            logger.info("Create a reduce action of Grammar " + (productionIndex + 1));
            newState.addAction(symbol,
                new ActionOperator(ActionOperator.OperatorType.Reduce, productionIndex + 1));
          }
        }
      }
    }
    // Set rest empty Acton and Goto to error.
    setErrorAction(newState);
    logger.info("New state is " + newState.toString(grammar));
    return newState;
  }

  private void setErrorAction(IState newState) {
    IAction action = newState.getStateAction();
    for (ISymbol symbol : grammar.getTermianlSymbol()) {
      // if there is no such symbol in the action.
      if (!action.findActionBySymbol(symbol)) {
        newState.addAction(symbol, new ActionOperator(ActionOperator.OperatorType.Error, -2));
      }
    }
    for (ISymbol symbol : grammar.getNonTerminalSymbol()) {
      // if there is no such symbol in the action.
      if (!action.findActionBySymbol(symbol)) {
        newState.addGoto(symbol, new ActionOperator(ActionOperator.OperatorType.Error, -2));
      }
    }
  }

  private int getIndexOfPointToSetOfItem(IItem item) throws Exception {
    for (int i = 0; i < allSetOfItemsCollector.size(); i++) {
      if (allSetOfItemsCollector.get(i).findItemInSet(item.getProduction(),
          item.getDotLocation() + 1)) {
        return i;
      }
    }
    throw new Exception("Error: there is no such Item in Set.");
  }

  @Override
  public void printTableDrivenParser() {
    logger.info("Get Table Driven Parser.");
    logger.info("   +  *  id (  )  $  |S  E  T  F");
    for (int i = 0; i < allSetOfItemsCollector.size(); i++) {
      if (i <= 9) {
        logger.info(i + "  " + parsingTable.get(i).toString(grammar));
      } else {
        logger.info(i + " " + parsingTable.get(i).toString(grammar));
      }
    }
  }

  @Override
  public HashMap<Integer, IState> getParsingTable() {
    return parsingTable;
  }

}
