package edu.pnw.compiler.parser.generator;

import java.util.HashMap;

import edu.pnw.compiler.parser.grammar.ISymbol;

public class Goto implements IGoto {
  HashMap<ISymbol, IActionOperator> gotoMap;

  @Override
  public HashMap<ISymbol, IActionOperator> getGotoMap() {
    return gotoMap;
  }

  public Goto() {
    gotoMap = new HashMap<>();
  }

  @Override
  public void addOperator(ISymbol symbol, IActionOperator actionOperator) {
    if (getGotoString(symbol) == null) {
      gotoMap.put(symbol, actionOperator);
    }
  }

  @Override
  public String getGotoString(ISymbol targetSymbol) {
    if (findGotoBySymbol(targetSymbol)) {
      return gotoMap.get(targetSymbol).toString();
    }
    return null;
  }

  @Override
  public boolean findGotoBySymbol(ISymbol targetSymbol) {
    for (ISymbol symbol : gotoMap.keySet()) {
      if (targetSymbol.equals(symbol)) {
        return true;
      }
    }
    return false;
  }

}
