package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.ISymbol;

public class State implements IState {
  private IAction stateAction;
  private IGoto stateGoto;

  public State() {
    stateAction = new Action();
    stateGoto = new Goto();
  }

  @Override
  public IAction getStateAction() {
    return stateAction;
  }

  @Override
  public IGoto getStateGoto() {
    return stateGoto;
  }

  @Override
  public void addAction(ISymbol currentSymbol, ActionOperator actionOperator) {
    stateAction.addOperator(currentSymbol, actionOperator);
  }

  @Override
  public void addGoto(ISymbol currentSymbol, ActionOperator actionOperator) {
    stateGoto.addOperator(currentSymbol, actionOperator);

  }

  @Override
  public String toString(IGrammar grammar) {
    String stateString = "";
    for (ISymbol symbol : grammar.getTermianlSymbol()) {
      String actionString = stateAction.getActionString(symbol);
        stateString += actionString;
    }
    stateString += "|";
    for (ISymbol symbol : grammar.getNonTerminalSymbol()) {
      String actionString = stateGoto.getGotoString(symbol);
        stateString += actionString;
    }
    return stateString;
  }

  @Override
  public HashMap<ISymbol, IActionOperator> getStateActionMap() {
    return stateAction.getActionMap();
  }

  @Override
  public HashMap<ISymbol, IActionOperator> getStateGotoMap() {
    return stateGoto.getGotoMap();
  }


}
