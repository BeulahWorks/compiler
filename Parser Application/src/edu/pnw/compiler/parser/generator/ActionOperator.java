package edu.pnw.compiler.parser.generator;

import edu.pnw.compiler.parser.generator.ActionOperator.OperatorType;

public class ActionOperator implements IActionOperator {

  private OperatorType operatorType;
  private int operatorIndex;

  @Override
  public int getOperatorIndex() {
    return operatorIndex;
  }

  @Override
  public OperatorType getOperatorType() {
    return operatorType;
  }

  public ActionOperator(OperatorType operatorType, int operatorIndex) {
    this.operatorType = operatorType;
    this.operatorIndex = operatorIndex;
  }

  public static enum OperatorType {
    Shift, Reduce, Accept, Error, Goto;
  }

  @Override
  public String toString() {
    if (operatorType.equals(ActionOperator.OperatorType.Shift)) {
      return ("S" + operatorIndex + " ").substring(0, 3);
    } else if (operatorType.equals(ActionOperator.OperatorType.Reduce)) {
      return "R" + operatorIndex + " ";
    } else if (operatorType.equals(ActionOperator.OperatorType.Goto)) {
      return (Integer.toString(operatorIndex) + "  ").substring(0, 3);
    } else if (operatorType.equals(ActionOperator.OperatorType.Accept)) {
      return "Acc";
    } else {
      return "-- ";
    }

  }
}
