package edu.pnw.compiler.parser.generator;

import java.util.HashMap;
import java.util.LinkedList;

import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.item.ISetOfItems;

public interface ITableDrivenParser {

  abstract void generateParser(LinkedList<ISetOfItems> allSetOfItemsCollector, IGrammar grammar)
      throws Exception;

  abstract void printTableDrivenParser();

  abstract HashMap<Integer, IState> getParsingTable();
}
