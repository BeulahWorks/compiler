package edu.pnw.compiler.parser.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.pnw.compiler.parser.grammar.Grammar;
import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.grammar.Production;
import edu.pnw.compiler.parser.grammar.Symbol;
import edu.pnw.compiler.parser.item.IItem;
import edu.pnw.compiler.parser.item.ISetOfItems;
import edu.pnw.compiler.parser.item.Item;
import edu.pnw.compiler.parser.item.SetOfItems;

public class ParserGenerator implements IParserGenerator {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private LinkedList<String> grammarList;

  private IGrammar grammar;

  private LinkedList<ISetOfItems> allSetOfItemsCollector;

  private ITableDrivenParser tableDrivenParser;

  @Override
  public void generateGrammar(String filename) throws Exception {

    GrammarFileReader grammarFileReadaer = new GrammarFileReader();

    /*
     * Read grammar from file.
     */
    if (("Grammar1").equals(filename)) {
      grammarList = grammarFileReadaer.readFile("src/GrammarSourceFile/Grammar1");
    } else if (("Grammar2").equals(filename)) {
      grammarList = grammarFileReadaer.readFile("src/GrammarSourceFile/Grammar2");
    } else {
      logger.error("Error when generating grammar.");
      throw new Exception("Error when generating grammar.");
    }
    /*
     * Generate Grammar
     */
    grammar = new Grammar();
    // The first line of Grammar file is Non-Terminal Symbol,
    // Create Symbol and store into Grammar.
    String[] inputLine = null;
    if (grammarList.size() < 3) {
      logger.error("Invalid grammar");
      throw new Exception("Invalid grammar");
    }
    inputLine = grammarList.get(0).split(",");
    for (String symbolValue : inputLine) {
      ISymbol symbol = new Symbol(Symbol.Type.NonTerminal, symbolValue);
      grammar.addNonTerminalSymbol(symbol);
      grammar.addSymbolToTable(symbolValue, symbol);
    }
    // The second line of Grammar file is Terminal Symbol,
    // Create Symbol and store into Grammar.
    inputLine = grammarList.get(1).split(",");
    for (String symbolValue : inputLine) {
      ISymbol symbol = new Symbol(Symbol.Type.Terminal, symbolValue);
      grammar.addTerminalSymbol(symbol);
      grammar.addSymbolToTable(symbolValue, symbol);
    }
    // Generate Productions.
    String productionString;
    ISymbol goalSymbol;
    LinkedList<ISymbol> productionContent;
    for (int i = 2; i < grammarList.size(); i++) {
      // ISymbol goalSymbol, LinkedList<ISymbol> productionContent
      productionString = grammarList.get(i);
      // goalSymbol = new Symbol(Symbol.Type.NonTerminal,
      // productionString.substring(0, 1));
      goalSymbol = grammar.getSymbolTable().get(productionString.substring(0, 1));
      logger.info("Start Production " + (i - 1) + "Analyz");
      logger.info("Analyze GoalSymbol " + productionString.substring(0, 1));
      productionContent = new LinkedList<>();
      // Single symbol in the production content.
      ISymbol contentSymbol;

      int j = 3;
      while (j < productionString.length()) {
        contentSymbol = grammar.getSymbolTable().get(productionString.substring(j, j + 1));
        logger.info("Analyze ContentSymbol " + productionString.substring(j, j + 1));
        if (contentSymbol != null) {
          productionContent.add(contentSymbol);
          j = j + 1;
        } else {
          contentSymbol = grammar.getSymbolTable().get(productionString.substring(j, j + 2));
          productionContent.add(contentSymbol);
          j = j + 2;
        }
      }
      grammar.addProduction(new Production(goalSymbol, productionContent));
    }
    // print the grammar
    String[] grammarString = grammar.printProductions();
    for (int i = 0; i < grammarString.length; i++) {
      logger.info(grammarString[i]);
    }
  }

  @Override
  public void itemGenerator(IGrammar grammar) throws Exception {
    logger.info("Start to generate Items.");
    // Create Collector for all SetOfItems.
    allSetOfItemsCollector = new LinkedList<>();
    // Add S0 SetOfItems to Collector.
    allSetOfItemsCollector.add(this.getS0SetofItem());
    logger.info("Add new setOfItems I0 into Collector.");
    ISetOfItems currentSetOfItems;
    ISetOfItems newSetOfItems;
    int sizeAllSetOfItems = allSetOfItemsCollector.size();
    for (int i = 0; i < sizeAllSetOfItems; i++) {
      // currentSetOfItems = setOfItemIterator.next();
      currentSetOfItems = allSetOfItemsCollector.get(i);
      for (ISymbol symbol : grammar.getAllSymbol()) {
        newSetOfItems = gotoNewSetOfItems(currentSetOfItems, symbol);
        logger.info("Finish GOTO function.");
        boolean addResult = false;
        // Only if the newSetOfItems is not empty, then try to add it
        // into collector.
        if (!newSetOfItems.getSetOfItems().isEmpty()) {
          logger.info("SetOfItems created by GOTO function is not empty.");
          addResult = addNewSetOfItemsToCollector(newSetOfItems);
        }
        if (addResult) {
          sizeAllSetOfItems += 1;
        }
      }
    }
  }

  private boolean addNewSetOfItemsToCollector(ISetOfItems newSetOfItems) {
    for (int i = 0; i < allSetOfItemsCollector.size(); i++) {
      if (newSetOfItems.compare(allSetOfItemsCollector.get(i))) {
        return false;
      }
    }
    logger.info("Add new setOfItems I" + (allSetOfItemsCollector.size()) + " into Collector.");
    allSetOfItemsCollector.add(newSetOfItems);

    return true;
  }

  @Override
  public ISetOfItems gotoNewSetOfItems(ISetOfItems setOfItems, ISymbol symbol) throws Exception {
    logger.info("Start GOTO function to find setOfItems: "
        + setOfItems.getSetOfItems().get(0).toString() + "with Symbol " + symbol.getSymbolVaule());
    ISetOfItems newSetOfItems = new SetOfItems();
    LinkedList<IItem> containSymbolItems = setOfItems.findItemsBySymbol(symbol);
    if (containSymbolItems.isEmpty()) {
      return newSetOfItems;
    }
    for (IItem item : containSymbolItems) {
      newSetOfItems.addItem(new Item(item.getProduction(), (item.getDotLocation() + 1)));
    }
    return this.closureSetOfItems(newSetOfItems);
  }

  @Override
  public ISetOfItems getS0SetofItem() throws Exception {
    IItem startItem = new Item(grammar.getProductionList().get(0), 0);
    ISetOfItems startSetOfItems = new SetOfItems();
    startSetOfItems.addItem(startItem);
    return closureSetOfItems(startSetOfItems);
  }

  @Override
  public ISetOfItems closureSetOfItems(ISetOfItems setOfItems) throws Exception {
    logger.info("Start Closure function");
    // New SetOfItems for return, clone input setOfItems.
    ISetOfItems newSetOfItems = new SetOfItems();
    newSetOfItems.getSetOfItems().addAll(setOfItems.getSetOfItems());
    /*
     * The production that the goal symbol in it match the non-terminal symbol after dot. Loop will
     * stop, when there's no more new item added.
     */
    int currentItemSize = newSetOfItems.getSetOfItems().size();
    for (int i = 0; i < currentItemSize; i++) {
      IItem currentItem = newSetOfItems.getSetOfItems().get(i);
      LinkedList<IProduction> productionList = null;
      // Get the Symbol after dot.
      ISymbol dotAfterSymbol =
          currentItem.getProduction().getSingleProductionContent(currentItem.getDotLocation());
      // If the current item is not null,
      // test whether it is non-terminal symbol
      // only if it is non-terminal,
      // the production list including this symbol as goal-symbol would be
      // obtained.
      if (dotAfterSymbol != null) {
        if (dotAfterSymbol.getSymbolType().equals(Symbol.Type.NonTerminal)) {
          productionList = grammar.findProductionsByGoalSymbol(dotAfterSymbol);
        }
      }
      // Try to add new Items into newSetOfItem.
      boolean hasItem;
      // The dot location of new item created by Closure should at 0.
      int dotForNewItem = 0;
      if (productionList != null) {
        for (IProduction production : productionList) {
          hasItem = newSetOfItems.findItemInSet(production, dotForNewItem);
          if (hasItem == false) {
            IItem newItem = new Item(production, dotForNewItem);
            newSetOfItems.addItem(newItem);
            currentItemSize += 1;
          }
        }
      }
    }
    String[] setOfItemString = newSetOfItems.printItems();
    logger.info("Complete closure function, get Item: ");
    for (int i = 0; i < setOfItemString.length; i++) {
      logger.info(setOfItemString[i]);
    }
    return newSetOfItems;
  }

  @Override
  public void generateTableDrivenParser(String parsingTable) throws Exception {
    if (parsingTable.equals("LR0")) {
      // generate tableDrivenParser of grammar1.
      logger.info("Start to generate Table Drive Parser LR0.");
      tableDrivenParser = new TableDrivenParserLR0();
      tableDrivenParser.generateParser(allSetOfItemsCollector, grammar);
    } else if(parsingTable.equals("LR1")){
      // generate tableDrivenParser of grammar2.
      logger.info("Start to generate Table Drive Parser LR1.");
      tableDrivenParser = new TableDrivenParserLR1();
      tableDrivenParser.generateParser(allSetOfItemsCollector, grammar);
    }
  }

  @Override
  public IGrammar getGrammar() {
    return grammar;
  }

  @Override
  public LinkedList<ISetOfItems> getAllSetOfItemsCollector() {
    return allSetOfItemsCollector;
  }

  @Override
  public ITableDrivenParser getTableDrivenParser() {
    return tableDrivenParser;
  }
}
