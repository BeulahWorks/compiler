package edu.pnw.compiler.parser.parsing;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.pnw.compiler.parser.generator.ActionOperator;
import edu.pnw.compiler.parser.generator.IActionOperator;
import edu.pnw.compiler.parser.generator.IState;
import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.grammar.IProduction;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.grammar.Symbol;

public class ParsingInput implements IParsingInput {
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  private HashMap<Integer, IState> parsingTable;
  private IGrammar grammar;

  public ParsingInput(IGrammar grammar, HashMap<Integer, IState> parsingTable) {
    this.grammar = grammar;
    this.parsingTable = parsingTable;
  }

  @Override
  public ActionOperator.OperatorType parsing(String inputString) throws Exception {
    Stack<ISymbol> input = paringStringToSymnbol(inputString);
    Stack<ISymbol> symbols = new Stack<ISymbol>();
    Stack<Integer> stack = new Stack<>();
    Stack<ISymbol> tempStack = new Stack<>();
    stack.push(0);
    IActionOperator actionOperator = null;
    IState currentState;
    ISymbol tempSymbol;
    IProduction currentProduction;
    ISymbol endSymbol = grammar.getSymbolTable().get("$");
    currentState = parsingTable.get(stack.peek());
    actionOperator = currentState.getStateActionMap().get(input.peek());
    while (true) {
      // get the top of stack.
      currentState = parsingTable.get(stack.peek());
      if (tempStack.isEmpty()) {
        actionOperator = currentState.getStateActionMap().get(input.peek());
        logger.info("(" + stack.peek() + "," + input.peek() + ") -->"
            + actionOperator.getOperatorType().toString() +" " + actionOperator.getOperatorIndex());
      } else {
        
        actionOperator = currentState.getStateGotoMap().get(tempStack.peek());
        logger.info("(" + stack.peek() + "," + tempStack.pop() + ") goto"
            + actionOperator.getOperatorIndex());
        stack.push(actionOperator.getOperatorIndex());
        // Based on goto index, change to new state.
        currentState = parsingTable.get(actionOperator.getOperatorIndex());
        actionOperator = currentState.getStateActionMap().get(input.peek());
        logger.info("(" + stack.peek() + "," + input.peek() + ") -->"
            + actionOperator.getOperatorType().toString()+" " + actionOperator.getOperatorIndex());
      }
      // if action is shift, pop one Symbol from input stack,
      // push this symbol into symbols stack,
      // push state index into stack.
      if (actionOperator.getOperatorType().equals(ActionOperator.OperatorType.Shift)) {
        stack.push(actionOperator.getOperatorIndex());
        if (!input.peek().equals(endSymbol)) {
          symbols.push(input.pop());
        }
      } else if (actionOperator.getOperatorType().equals(ActionOperator.OperatorType.Reduce)) {
        currentProduction = grammar.getProductionByIndex(actionOperator.getOperatorIndex());
        logger.info(currentProduction.toString());
        for (int i = currentProduction.getAllProductionContent().size() - 1; i >= 0; i--) {
          if (!symbols.pop().equals(currentProduction.getAllProductionContent().get(i))) {
            logger.error("Error: the reduce function has a problem.");
            throw new Exception("Error: the reduce function has a problem.");
          }
          // pop symbol from stack once, pop one item from stack.
          stack.pop();
        }
        // push goal symbol into stack.
        tempSymbol = currentProduction.getGoalSymbol();
        symbols.push(tempSymbol);
        tempStack.push(tempSymbol);
      }
      logger.info("Stack: " + stack.toString() + " Symbols: " + symbols.toString() + " Action: "
          + actionOperator.getOperatorType().toString());
      logger.info("---------------------------------------");
      if (actionOperator.getOperatorType().equals(ActionOperator.OperatorType.Accept)
          || actionOperator.getOperatorType().equals(ActionOperator.OperatorType.Error)) {
        return actionOperator.getOperatorType();
      }
    }
  }

  @Override
  public Stack<ISymbol> paringStringToSymnbol(String inputString) throws Exception {
    logger.info("Start parsing input to Symbol.");
    Stack<ISymbol> inputStack = new Stack<>();
    ISymbol contentSymbol;
    ISymbol goalSymbol;
    int indexString = 0;
    char nextChar;
    boolean addIdSymbol = false;
    while (indexString < inputString.length()) {
      nextChar = (char) inputString.charAt(indexString);
      if ((!Character.isLetterOrDigit(nextChar)) && addIdSymbol) {
        // if this char is not character, then add previous as
        inputStack.push(grammar.getSymbolTable().get("id"));
        contentSymbol =
            grammar.getSymbolTable().get(inputString.substring(indexString, indexString + 1));
        // if can not find this terminal symbol, throw error.
        if (contentSymbol == null) {
          logger.info("Unreadable input of " + inputString.substring(indexString, indexString + 1));
          throw new Exception(
              "Unreadable input of " + inputString.substring(indexString, indexString + 1));
        }
        inputStack.push(contentSymbol);
        addIdSymbol = false;
      } else {
        addIdSymbol = true;
      }
      indexString++;
    }
    if (addIdSymbol) {
      inputStack.push(grammar.getSymbolTable().get("id"));
    }
    logger.info("Finish Parsing Input to symbol: " + inputStack.toString());
    Stack<ISymbol> returnStack = new Stack<>();
    returnStack.push(grammar.getSymbolTable().get("$"));
    while (!inputStack.isEmpty()) {
      returnStack.push(inputStack.pop());
    }
    logger.info("Revise" + returnStack.toString());
    return returnStack;
  }

}
