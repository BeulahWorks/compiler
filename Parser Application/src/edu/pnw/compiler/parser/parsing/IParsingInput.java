package edu.pnw.compiler.parser.parsing;

import java.util.Stack;

import edu.pnw.compiler.parser.generator.ActionOperator;
import edu.pnw.compiler.parser.grammar.ISymbol;
import edu.pnw.compiler.parser.grammar.Symbol.Type;

public interface IParsingInput {
  abstract Stack<ISymbol> paringStringToSymnbol(String inputString) throws Exception;

  abstract ActionOperator.OperatorType parsing(String inputString) throws Exception;
}
