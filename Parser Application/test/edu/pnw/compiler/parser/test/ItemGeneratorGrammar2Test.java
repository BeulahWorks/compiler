package edu.pnw.compiler.parser.test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import edu.pnw.compiler.parser.generator.IParserGenerator;
import edu.pnw.compiler.parser.generator.ParserGenerator;
import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.item.ISetOfItems;

public class ItemGeneratorGrammar2Test {

  @Test
  public void test() throws Exception {
    IParserGenerator parserGenerator = new ParserGenerator();
    parserGenerator.generateGrammar("Grammar2");
    IGrammar grammar = parserGenerator.getGrammar();
    grammar.printProductions();
    parserGenerator.itemGenerator(grammar);
    LinkedList<ISetOfItems> allSetOfItems = parserGenerator.getAllSetOfItemsCollector();
    int setIndex = 0;
    for(ISetOfItems setOfItem : allSetOfItems){
      
      String[] setOfItemString = setOfItem.printItems();
      System.out.println("I " + setIndex);
      for(int i = 0; i< setOfItemString.length;i++){       
        System.out.println(setOfItemString[i]);        
      }
      System.out.println(" ");
      setIndex++;
    }
  }

}
