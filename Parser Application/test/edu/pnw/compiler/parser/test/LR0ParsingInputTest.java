package edu.pnw.compiler.parser.test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import edu.pnw.compiler.parser.generator.IParserGenerator;
import edu.pnw.compiler.parser.generator.ITableDrivenParser;
import edu.pnw.compiler.parser.generator.ParserGenerator;
import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.item.ISetOfItems;

public class LR0ParsingInputTest {

  @Test
  public void test() throws Exception {
    IParserGenerator parserGenerator = new ParserGenerator();
    parserGenerator.generateGrammar("Grammar1");
    IGrammar grammar = parserGenerator.getGrammar();
    grammar.printProductions();
    parserGenerator.itemGenerator(grammar);
    LinkedList<ISetOfItems> allSetOfItems = parserGenerator.getAllSetOfItemsCollector();
    parserGenerator.generateTableDrivenParser("LR0");
    ITableDrivenParser tableDrivenParser = parserGenerator.getTableDrivenParser();
    
    tableDrivenParser.getParsingTable();

  }

}
