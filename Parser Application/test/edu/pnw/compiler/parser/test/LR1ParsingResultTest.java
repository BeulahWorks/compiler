package edu.pnw.compiler.parser.test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import edu.pnw.compiler.parser.generator.IParserGenerator;
import edu.pnw.compiler.parser.generator.ITableDrivenParser;
import edu.pnw.compiler.parser.generator.ParserGenerator;
import edu.pnw.compiler.parser.grammar.IGrammar;
import edu.pnw.compiler.parser.item.ISetOfItems;
import edu.pnw.compiler.parser.parsing.IParsingInput;
import edu.pnw.compiler.parser.parsing.ParsingInput;

public class LR1ParsingResultTest {

  @Test
  public void test() throws Exception {
    IParserGenerator parserGenerator = new ParserGenerator();
    parserGenerator.generateGrammar("Grammar2");
    IGrammar grammar = parserGenerator.getGrammar();
    grammar.printProductions();
    parserGenerator.itemGenerator(grammar);
    LinkedList<ISetOfItems> allSetOfItems = parserGenerator.getAllSetOfItemsCollector();
    parserGenerator.generateTableDrivenParser("LR1");

    ITableDrivenParser tableDrivenParser = parserGenerator.getTableDrivenParser();

    tableDrivenParser.printTableDrivenParser();

    IParsingInput parsingInput = new ParsingInput(grammar, tableDrivenParser.getParsingTable());

    // Stack<ISymbol> symbolList = parsingInput.paringStringToSymnbol("9*7+6");
    System.out.println(parsingInput.parsing("9*7+6").toString());
    System.out.println(parsingInput.parsing("9+7*6").toString());
  }

}
